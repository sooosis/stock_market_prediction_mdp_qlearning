import random
import numpy as np
from collections import defaultdict
from Functions import Get_Actions
from State import state
import os


class MDP:

    def __init__(self, init, gamma , fgama ,terminal , state):
        if not (0 < gamma <= 1):
            raise ValueError("An MDP must have 0 < gamma <= 1")


        self.init = init

        self.gamma = gamma

        self.fgama = fgama

        self.phi = float(np.power(1-fgama , -1))


    def R(self, state):

        return self.Ut(state.c)

    def T(self, state, action):

        return 1

    def actions(self, state):

        return Get_Actions(state.y , state.m , state.d)

    def investment_vector(self):


    def Ut(self , x):
        return (1/self.fgama)*np.power(x,self.fgama)


    def Up(self , x):
        return (1/self.fgama)*np.power(x,self.fgama)


    def Consumption(self , dn , x):
        phi = self.phi
        fgama = self.fgama
        return (np.power(fgama*dn ,-phi))*x



